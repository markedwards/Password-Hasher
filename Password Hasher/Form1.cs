﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;

namespace Password_Hasher
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Hash.Text = Service.Text.Trim();
            if (Hash.Text != "") {
                Hash.Text += " : ";
            }
            String full = sha256_hash(Password.Text.Trim());
            Hash.Text += full.Substring(0,16) + " " + full.Substring(48,16);
            Service.Text = "";
            Password.Text = "";
            Clipboard.SetText(Hash.Text + "\r\n");
        }

        public static String sha256_hash(String value)
        {
            using (SHA256 hash = SHA256Managed.Create())
            {
                return String.Concat(hash
                  .ComputeHash(Encoding.UTF8.GetBytes(value))
                  .Select(item => item.ToString("x2")));
            }
        }
    }

}
